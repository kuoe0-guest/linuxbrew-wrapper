Source: linuxbrew-wrapper
Section: contrib/utils
Priority: optional
# according to Policy 2.5, I think users should be aware
# what they are doing invoking linuxbrew, so I think it should be 'extra'.
Maintainer: Mo Zhou <cdluminate@gmail.com>
Build-Depends: debhelper (>=10), gem2deb
Standards-Version: 4.1.4
Homepage: http://linuxbrew.sh/
Vcs-Browser: https://salsa.debian.org/lumin-guest/linuxbrew-wrapper
Vcs-Git: https://salsa.debian.org/lumin-guest/linuxbrew-wrapper.git

Package: linuxbrew-wrapper
Architecture: all
# Set Arch to 'all', see #799390
Multi-Arch: foreign
Depends: ${misc:Depends}, ${ruby:Depends},
         build-essential,
         curl,
         git,
         ruby,
         python-setuptools,
Description: Homebrew package manager for Linux
 Linuxbrew is a fork of Homebrew, the Mac OS package manager, for Linux.
 .
 It can be installed in your home directory and does not require root
 access. The same package manager can be used on both your Linux server
 and your Mac laptop. Installing a modern version of glibc and gcc in
 your home directory on an old distribution of Linux takes five minutes.
 .
 Features:
  * Can install software to a home directory and so does not require sudo
  * Install software not packaged by the native distribution
  * Install up-to-date versions of software when the native distribution is old
  * Use the same package manager to manage both your Mac and Linux machines
 .
 This package provides Linuxbrew install scripts instead of linuxbrew itself.
